﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using CSharpEditor;

namespace WrapCSharpOne
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string fc = @"
namespace Button_139_1_1_et_ITEM_PRESSED_COS_NAMESPACE
{
    using System;
    using SAPbobsCOM;
    using SAPbouiCOM;
    using CosineSystemInfra;

public class Button_139_1_1_et_ITEM_PRESSED
    {
        public static bool OnBeforeItemPressed(ItemEvent pVal)
        {
            Form form = B1Connections.theAppl.Forms.Item(pVal.FormUID);
            B1Connections.theAppl.SetStatusBarMessage(""Project Code COS already exists in the system."", BoMessageTime.bmt_Medium, false);
            return false;
        }
    }
}";
            MainForm f1 = Wrap.Execute();
            f1.Show();
        }
    }
}
